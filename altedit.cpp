#include "altedit.h"

AltEdit::AltEdit(QWidget *parent)
    : QWidget(parent)
{
    this->setWindowTitle("AltEdit");
    m_tEdit =new QTextEdit();
    
    m_bar =new QMenuBar(this);
        lay_this =new QVBoxLayout;
        lay_this->addWidget(m_bar);
        lay_this->addWidget(m_tEdit);
    this->setLayout(lay_this);
    CreateMenu();
    
}

AltEdit::~AltEdit()
{
    
}

void AltEdit::CreateMenu()
{
    QMenu *fileMenu = m_bar->addMenu("&File");
    
    QAction *f_load =new QAction(tr("&Open"), fileMenu);
    fileMenu->addAction(f_load);
    connect(f_load, SIGNAL(triggered(bool)), this, SLOT(FileOpen()));
    
    QAction *f_close =new QAction(tr("&Close"), fileMenu);
    fileMenu->addAction(f_close);
    connect(f_close, SIGNAL(triggered(bool)), this, SLOT(FileClose()));
    
    QAction *f_save =new QAction(tr("&Save"), fileMenu);
    fileMenu->addAction(f_save);
    connect(f_save, SIGNAL(triggered(bool)), this, SLOT(FileSave()));
    
    QMenu *editMenu = m_bar->addMenu("&Edit");
    
    QMenu *e_Add =editMenu->addMenu("Add");
    
    QAction *eA_table =new QAction(tr("Table"), e_Add);
    e_Add->addAction(eA_table);
    connect(eA_table, SIGNAL(triggered(bool)), this, SLOT(AddTable()));
    
    QAction *eA_img =new QAction(tr("Image"), e_Add);
    e_Add->addAction(eA_img);
    connect(eA_img, SIGNAL(triggered(bool)), this, SLOT(AddImage()));
    
    QAction *e_font =new QAction(tr("&Font"), editMenu);
    editMenu->addAction(e_font);
    connect(e_font, SIGNAL(triggered(bool)), this, SLOT(FontEdit()));
    
    QAction *e_color =new QAction(tr("Color"), editMenu);
    editMenu->addAction(e_color);
    connect(e_color, SIGNAL(triggered(bool)), this, SLOT(ColorEdit()));
    
    
    QMenu *e_Align =editMenu->addMenu("Align");
    
    QAction *e_aLeft =new QAction(tr("Left"), e_Align);
    e_Align->addAction(e_aLeft);
    connect(e_aLeft, SIGNAL(triggered(bool)), this, SLOT(AlignEditLeft()));
    
    QAction *e_aRight =new QAction(tr("Right"), e_Align);
    e_Align->addAction(e_aRight);
    connect(e_aRight, SIGNAL(triggered(bool)), this, SLOT(AlignEditRight()));
    
    QAction *e_aCenter =new QAction(tr("Center"), e_Align);
    e_Align->addAction(e_aCenter);
    connect(e_aCenter, SIGNAL(triggered(bool)), this, SLOT(AlignEditCenter()));
}

void AltEdit::FileOpen()
{
    UrlFile =QFileDialog::getOpenFileName(this, tr("Open As"), "", "Text doc (*.htm *.html *.js *.json *.txt)");
    
    QFile file(UrlFile);
    
    if(file.open(QIODevice::ReadOnly)){
        m_tEdit->setText(tr(file.readAll()));
        this->setWindowTitle(QString("AltEdit -%1").arg(QFileInfo(file.fileName()).fileName()));
        file.close();
    }
}

void AltEdit::FileClose()
{
    UrlFile.clear();
    m_tEdit->clear();
}

void AltEdit::FileSave()
{
    
    QPrinter printer;
     printer.setOutputFormat(QPrinter::PdfFormat);
     printer.setOutputFileName(QFileDialog::getSaveFileName(this, tr("Save As"), "", "Print (*.pdf)"));
     m_tEdit->document()->print(&printer);
     
}

void AltEdit::FontEdit()
{
    QTextCharFormat format =m_tEdit->textCursor().charFormat();
    format.setFont(QFontDialog::getFont(0, this->font()));
    m_tEdit->textCursor().setCharFormat(format);
}

void AltEdit::ColorEdit()
{
    QTextCharFormat format =m_tEdit->textCursor().charFormat();
    format.setBackground(QColorDialog::getColor());
    m_tEdit->textCursor().setCharFormat(format);
}

void AltEdit::AlignEditLeft()
{
    QTextCursor cursor = m_tEdit->textCursor();
    QTextBlockFormat textBlockFormat = cursor.blockFormat();
    textBlockFormat.setAlignment(Qt::AlignLeft);
    cursor.mergeBlockFormat(textBlockFormat);
    m_tEdit->setTextCursor(cursor);
}

void AltEdit::AlignEditRight()
{
    QTextCursor cursor = m_tEdit->textCursor();
    QTextBlockFormat textBlockFormat = cursor.blockFormat();
    textBlockFormat.setAlignment(Qt::AlignRight);
    cursor.mergeBlockFormat(textBlockFormat);
    m_tEdit->setTextCursor(cursor);
}

void AltEdit::AlignEditCenter()
{
    QTextCursor cursor = m_tEdit->textCursor();
    QTextBlockFormat textBlockFormat = cursor.blockFormat();
    textBlockFormat.setAlignment(Qt::AlignCenter);
    cursor.mergeBlockFormat(textBlockFormat);
    m_tEdit->setTextCursor(cursor);
}

void AltEdit::AddTable()
{
    m_tEdit->textCursor().insertTable(5,5);
}

void AltEdit::AddImage()
{
    QImage img;
    img.load(QFileDialog::getOpenFileName(this, "Open Img", "", "Image(*.jpg *.png *.bmp)"));
    img =img.scaledToHeight(200);
    m_tEdit->textCursor().insertImage(img);
}
