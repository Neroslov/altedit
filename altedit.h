#ifndef ALTEDIT_H
#define ALTEDIT_H

#include <QWidget>
#include <QMenuBar>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QFileDialog>
#include <QFile>
#include <QKeyEvent>
#include <QFontDialog>
#include <QColorDialog>
#include <QTextCursor>
#include <QPrinter>

class AltEdit : public QWidget
{
    Q_OBJECT
    
public:
    AltEdit(QWidget *parent = 0);
    ~AltEdit();
private:
    QString UrlFile;
    QTextEdit *m_tEdit;
    QMenuBar *m_bar;
    QVBoxLayout *lay_this;
private slots:
    void CreateMenu();
    void FileOpen();
    void FileClose();
    void FileSave();
    void FontEdit();
    void ColorEdit();
    void AlignEditLeft();
    void AlignEditRight();
    void AlignEditCenter();
    void AddTable();
    void AddImage();
};

#endif // ALTEDIT_H
